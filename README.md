TwiXt - Templated Widgets for Xt
================================

TwiXt is intended to make it easier to write widgets for X, using the X
Toolkit Intrinsics. Doing so by hand is not really difficult, but it is
a bit tedious.
The Xt widgets are based on the idea of classes with single inheritance.
On the other hand, the implementation language C is not object-oriented,
so there is lots of work for the library to do and lots of details for
the widget implementer to write.

If you specify a widget (class) named FooThing, TwiXt will generate for
you the public header file `FooThing.h`, the private header file
`FooThingP.h`, and the top part of `FooThing.c`, with the definitions of
the resource records, the class record and functions that have pointers
to them. 

TwiXt uses all naming conventions that the Xt and Xaw widgets as far as
possible. It turns out that in some cases those widgets themselves
deviate from their own conventions. In that case, TwiXt attempts to have
a mechanism to specify that kind of exception to the rules.

Because C does not have a class system built-in, this is instead
implemented in the Intrinsics library. This makes use of a
user-specified collection of fields (or sometimes called members) that
make up the class. Those fields are basically common for all instances
of the widget.

Then there are the fields that exist once for each widget instance.
Therefore the outline of a widget description generally looks like this:

<pre><code>
widget Name : BaseClass {
    class {
        # fields ...
    }
    instance {
        # fields ...
    }
};
</code></pre>

For more details on this, please see the [X Toolkit Intrinsics
documentation](http://www.x.org/releases/X11R7.7/doc/libXt/intrinsics.html).
TwiXt aims to make use of Xt easier, but fully explaining it is beyond
its scope.

See also some [Free O'Reilly
books](http://www.x.org/wiki/ProgrammingDocumentation/) and client
development documentation on [the X.org
website](http://www.x.org/releases/X11R7.7/doc/index.html#client-devel).

Class fields
------------

The class fields are always private, i.e. are only under control of the
widget's code (and the Intrinsics library of course), and not accessible
to the user's code.  Instance fields on the other hand, can potentially
be accessed by the user. This is the case for the fields that correspond
to a Resource. In addition to those, there can of course also be private
fields.

<pre><code>
    class {
        WidgetClass superclass = (WidgetClass)&unnamedObjClassRec; sub= (WidgetClass)&%lsClassRec; 
                                                          /* pointer to superclass ClassRec   */
        String      class_name = "%c";                    /* widget resource class name       */
        Cardinal    widget_size = sizeof(%cRec);          /* size in bytes of widget record   */
        XtProc      class_initialize = %cClassInitialize; /* class initialization proc        */
        # ...
    }
</code></pre>

Class field descriptions look very much like normal C declarations.
There are some general conventions:

- C types are spelled in CamelCase (starting with an uppercase letter).
- field names are spelled in lower_case (with underscores).

Where needed, these forms can seamlessly be converted into each other.
This happens for instance when a function name is generated from a field
name.

A field description can optionally include initalization values in two
variants: one for class records in the class implementation itself,
(started with `=`) and one proposed to implementations of subclasses
(start with `sub=`).
The subclass can override the proposal if it wants.

If no class expression is given, it usually defaults to 0.
If no subclass expression is given, it defaults to the class expression.

TwiXt doesn't really analyze the structure of your expressions (the C
compiler will complain later if you write nonsense),
but there is some pattern expansion to fill in class names.

- `%c` will be replaced by the `ClassName` of the class where it is used.
- `%s` will be replaced by the `ClassName` of the superclass.
- `%lc` and `%ls` will be that name, with a lowercase initial
- `%_c` and `%_s` will be that name, in `lower_cased` form.

Using these expansions can help you to propose appropriate function
names in subclasses that are unique and descriptive.

Following the class field, you can put a C-style comment.
It will be copied into the generated source.
These comments are only possible in designated locations.
TwiXt-style comments are allowed anywhere. They start with a hash symbol
`#` and continue to the end of the line.

### Function pointers

For class fields that represent a C function pointer (i.e. their type
name ends in `Proc`, `Func`, `Handler` or `Converter`, or a simplistic
pattern matching on the declaration succeeds), the following code
fragments are generated.

For use in subclasses, a `#define XtInherit`_Field_ will be produced.
Also, if the value which is used in the C initializer looks like a
function name (is in proper CamelCase), a function declaration and
definition will be generated.
(Exception: if the initialisation value contains the word `Inherit` then
that name will be used for the `#define` instead. In that case also
there will be no function declaration and body for this name.)

The signature of the functions is based on a known list of type names (derived
from the header files `<X11/IntrinsicP.h>` and `<X11/Intrinsic.h>`) and some
pattern matching for other cases. There is no real parser of C declarations or
expressions, so this process can be fooled.

The function body can be given in a block `code "FunctionName" {{{ ...
}}}`.
Otherwise it wil be empty.

### Class field overrides

When a subclass is created, the superclass' fields are included in the
subclass. As mentioned, by default they get the value as proposed by its
own class. However you can override that with an `override` block before
your own `class` block (because that is the order in which the full class
record is generated):

<pre></code>
widget SubClass : BaseClass {
    override BaseClass {
        # We would like a different value for our version of the
        # class field BaseClass.classfield_int.
        classfield_int = 99;
    }
    class {
        # ...
</code></pre>

It is checked that `BaseClass` is indeed a superclass of the current
widget, an that `classfield_int` is one of its class fields.
The same class name expansions are available for the value here too.

Instance fields
---------------

As mentioned, instance fields can potentially be accessed by the user.
This is the case for the fields that correspond to a Resource. In
addition to those, there can of course also be private fields.
Here are some examples of both cases:

<pre></code>
widget Root {
    instance {
        private: Widget         self;               /* pointer to widget itself */
        public:  Pixel(BorderColor) border_color
                  =R(String) "XtDefaultForeground"; /* window border pixel      */
</code></pre>

Private fields are the simplest. They are much like a C field
declaration. There is not even an initial value like with class fields,
because there is no static instance defined. The `initialize` function
pointer (inherited from the `Core` widget) is meant to set fields in a
new instance to their initial values.

Public fields are a bit more involved, since they basically are a
combination of an instance field and a resource description. The latter
conststs of 7 parts but fortunately you can usually leave some of them
out.

Resources consist of the following parts:

- resource_name:
  The name to use with XtSetValues() and XtGetValues() to access
  the resource (or with XtCreateWidget() or in a resource file).
  This is in principle an arbitrary string, but
  conventionally it is in camelCase (lower case initial).
  This name is actually generated from the instance field name,
  which is in lower_case_style.
  A `#define` is generated with the same name: `XtN`_resourceName_.
  It is usual to use the define instead of the literal string.
  This prevents typos in the name.

  In the example, this is `border_color`.

- resource_class:
  Not to be confused with "class" as in the meaning of "type", but
  a name to group multiple related resources together. The aim is that
  the user can easily set all Background-related colours to the same
  value in a resource file. Resource classes are spelled in CamelCase
  (upper case initial).
  A `#define` is generated with the same name: `XtN`_ClassName_.

  In the example, this is `BorderColor`.

- resource_type:
  This is the name of the type which represents the value of the
  resource. Xt type converters know of these names to convert for
  instance strings into the correct values.
  A `#define` is generated with the same name: `XtR`_ResourceType_.

  In the example, this is `Pixel`.

- resource_size:
  This is the size of objects of type `resource_type`. Normally this
  is derived from the `resource_type` field. If you want to make an
  exception, use `: ctypename` after the `resource_name`. This type is
  then also used in generating the instance field.

- resource_offset:
  This describes where in the instance record the resource is located.
  It is pretty much always derived automatically from the instance field
  (which in turn is derived from `resource_name`).  
  In the rare cases that you have a resource which does not have a
  one-to-one correspondence with an instance field, use this
  construction as seen in the CoreWidget:
  <pre><code>
    private:  XtTMRec       tm;                 /* translation management  */
    resource: TranslationTable(Translations) translations <b>@tm.translations</b>
                     =R(TranslationTable) NULL; /* translation management  */
   </code></pre>

  Here, the instance contains a `XtTMRec` structure, and the resource
  refers to one if its internal fields.
  By using the `resource` keyword instead of `public`, you indicate
  that no field is generated to go with the resource. Use an
  appropriate other field instead. It also allows the use of the `@`
  which is not allowed for `public`. The field is usually in the
  widget's own instance part record. If you want to specify the full
  field name in the instance record, prefix the it with `:`.

- default_type:
  Resources can be initialized by the resource system automatically.
  This field specifies the type of `default_addr`.

  In the example, this is `String` (transformed into `XtRString`).

  There is special syntax for the special types
    * `XtRImmediate` (which copies the value without converting it):
      just use a simple `=` sign
    * `XtRCallProc` (which calls a function of signature
      [`XtResourceDefaultProc`](http://www.x.org/releases/X11R7.7/doc/libXt/intrinsics.html#XtResourceDefaultProc)):
      use `= FunctionName()`.

- default_addr:
  This should point to an appropriate value of the type named by
  `default_type`.
  When the Intrinsics library initializes the field, it will normally
  use a converter from the default_type to the resource_type.

  In the example, this is `"XtDefaultForeground"`.

Summarizing, a resource description that uses no defaults would look
like

    resource: ResourceType(ResourceClass) resource_name :resource_size_type @resource_offset
                     =R(DefaultType) default_addr;

For more details, see [chapter 9 "Resource Management" of the Intrinsics
document](http://www.x.org/releases/X11R7.7/doc/libXt/intrinsics.html#Resource_Management).

Generating function declarations or definitions for instance fields
which point to such isn't very useful, so it isn't done. This is because
in class fields, they typically point to a fixed function, but if you
have them in instances, that would not be useful. You'd have multiple
alternative functions to point to. And that is getting out of TwiXt's
scope (and ability to invent syntax for it).

## Dependencies

TwiXt uses Perl and the non-included module Parser::MGC.

## Examples

Here is a simple example input file, followed by the output files.

### testfile.xt
<pre><code>
#
# Compile with gcc -I. -I/usr/X11R7/include -c FooThing.c
#
# The widget FooThing is based on widget BaseType.
# That means that it adds to both its class fields and its instance fields.

widget FooThing : BaseType {
    code "top_h_file" {{{
        /* top_h_file */
    }}}
    code "bottom_h_file" {{{
        /* bottom_h_file */
    }}}
    code "top_ph_file" {{{
        /* top_ph_file */
    }}}
    code "bottom_ph_file" {{{
        /* bottom_ph_file */
    }}}
    code "top_c_file" {{{
        /* top_c_file */
    }}}
    code "bottom_c_file" {{{
        /* bottom_c_file */
    }}}
    override BaseType {
	# We would like a different value for our version of the
	# class field BaseType.classfield_int.
	classfield_int = 99;
    }
    # These are our own class fields
    class {
	int foo_thing_int = 0xF001;
	long foo_thing_long = 0xF002;
	void (*funcptr)(int /* a */, int /* b */) = PointedToFunc;
        XtWidgetProc test_func = TestFunc;
    }
    class-extension {
        int NULLQUARK; /* default extension record name is NULLQUARK */
    }
    class-extension FooQuark 3 {
        XtWidgetProc func_in_extension = ExtensionversionThree;
    }
    code "TestFunc" {{{
        /* This is to be the body of the function pointed to by WidgetProc test_func */
        return NULL;
    }}}
    # And these are our instance fields.
    # public: reprtype(resourceclass) field = ctype: init;
    instance {
	public: Pixel(Color) foreground = 0;	/* comment on foreground */
	public: Pixel(Color) background = (0 + 0) / 2;
	public: Int(Int) testint : int = 3;
	private: long int long_int_field;
    }
};

widget BaseType : Core {
    class {
	# For class fields you can specify a different default value
	# for derived classes. Typically this would be a special
	# "inherit from superclass" value such as XtInheritDestroy.
	int  classfield_int  = 1; sub= 2;
	long classfield_long = 3; sub= 4;

	# If the type ends in Proc or Func, #define ...InheritWibble ...
	# If the subclass init value contains Inherit, that name is used
	# instead of BaseTypeInheritWibble (class + Inherit + field name).
	XtWidgetProc wibble = BaseTypeWibble; sub= NonDefaultNameForInheritWibble;
	# %c is replaced here by the class name.
	XtWidgetProc frubble = %cFrubble; sub= %cFrubble;
    }
    instance {
    }
};

# %include "athena.xt"
</code></pre>


### BaseType.h

<pre><code>
/*
 * Do not edit this file.
 * It has been generated by TwiXt
 * from widget description testfile.xt.
 */
#ifndef BASETYPE_H
#define BASETYPE_H

#include &lt;X11/Intrinsic.h>
#include &lt;X11/StringDefs.h>


/* Resource names */

/* Resource classes */

/* Resource representation types */


/* Class record pointer */
extern WidgetClass baseTypeWidgetClass;

/* C Widget type definition */
typedef struct BaseTypeClassRec *BaseTypeWidgetClass;
typedef struct BaseTypeRec *BaseTypeWidget;

/* New class method entry points */


#endif /* BASETYPE_H */
</code></pre>

### BaseTypeP.h

<pre><code>
/*
 * Do not edit this file.
 * It has been generated by TwiXt
 * from widget description testfile.xt.
 */
#ifndef BASETYPEP_H
#define BASETYPEP_H

#include &lt;X11/IntrinsicP.h>

/* Include public header */
#include &lt;BaseType.h>




/* New representation types used by the BaseType widget */

/* Declarations for class functions */
extern void BaseTypeWibble(Widget /* widget */);
extern void BaseTypeFrubble(Widget /* widget */);


/* Defines for inheriting superclass function pointer values */
#ifndef NonDefaultNameForInheritWibble
# define NonDefaultNameForInheritWibble ((XtWidgetProc) _XtInherit)
#endif
#ifndef XtInheritFrubble
# define XtInheritFrubble ((XtWidgetProc) _XtInherit)
#endif


/* New fields for the BaseType instance record */
typedef struct {
    /* Settable resources and private data */

} BaseTypePart;

/* Full instance record declaration */
typedef struct BaseTypeRec {
    BaseTypePart base_type;

} BaseTypeRec;

/* Types for BaseType class methods */

/* New fields for the BaseType class record */
typedef struct {
    int classfield_int; 
    long classfield_long; 
    XtWidgetProc wibble; 
    XtWidgetProc frubble; 

} BaseTypeClassPart;

/* Class extension records */


/* Full class record declaration */
typedef struct BaseTypeClassRec {
    BaseTypeClassPart base_type_class;

} BaseTypeClassRec;

extern struct BaseTypeClassRec baseTypeClassRec;


#endif /* BASETYPEP_H */
</code></pre>

### BaseType.c

<pre><code>
/*
 * Do not edit this file.
 * It has been generated by TwiXt
 * from widget description testfile.xt.
 */

#include &lt;stddef.h>
#include &lt;BaseTypeP.h>


/******************************************************************
 *
 * BaseType Resources
 *
 ******************************************************************/

static XtResource resources[] = {

};

struct BaseTypeClassRec baseTypeClassRec = {
    { /* BaseType for BaseType */
    .classfield_int = 1, 
    .classfield_long = 3, 
    .wibble = BaseTypeWibble, 
    .frubble = BaseTypeFrubble, 
    }, /* BaseType */

};

/*
 * Declare this as WidgetClass instead of the "more real" types
 *
 *     BaseTypeClassRec * or BaseTypeClassRec *,
 *
 * because Xt functions such as XtCreateWidget() take an argument of that type.
 *
 * The definition of WidgetClass in &lt;X11/Core.h> is
 *
 *     typedef struct _WidgetClassRec *CoreWidgetClass;
 *
 * where Widget is a strange alias of Core.
 */
WidgetClass baseTypeWidgetClass = (WidgetClass)&baseTypeClassRec.base_type_class;

/* Definitions for class functions */
void BaseTypeWibble(Widget  widget )
{

}

void BaseTypeFrubble(Widget  widget )
{

}
</code></pre>

### FooThing.h

<pre><code>
/*
 * Do not edit this file.
 * It has been generated by TwiXt
 * from widget description testfile.xt.
 */
#ifndef FOOTHING_H
#define FOOTHING_H

#include &lt;X11/Intrinsic.h>
#include &lt;X11/StringDefs.h>

/* top_h_file */
/* Resource names */
#ifndef XtNforeground
# define XtNforeground "foreground"
#endif
#ifndef XtNbackground
# define XtNbackground "background"
#endif
#ifndef XtNtestint
# define XtNtestint "testint"
#endif

/* Resource classes */
#ifndef XtCColor
# define XtCColor "Color"
#endif
#ifndef XtCColor
# define XtCColor "Color"
#endif
#ifndef XtCInt
# define XtCInt "Int"
#endif

/* Resource representation types */
#ifndef XtRPixel
# define XtRPixel "Pixel"
#endif
#ifndef XtRPixel
# define XtRPixel "Pixel"
#endif
#ifndef XtRInt
# define XtRInt "Int"
#endif


/* Class record pointer */
extern WidgetClass fooThingWidgetClass;

/* C Widget type definition */
typedef struct FooThingClassRec *FooThingWidgetClass;
typedef struct FooThingRec *FooThingWidget;

/* New class method entry points */

/* bottom_h_file */
#endif /* FOOTHING_H */
</code></pre>

### FooThingP.h

<pre><code>
/*
 * Do not edit this file.
 * It has been generated by TwiXt
 * from widget description testfile.xt.
 */
#ifndef FOOTHINGP_H
#define FOOTHINGP_H

#include &lt;X11/IntrinsicP.h>

/* Include public header */
#include &lt;FooThing.h>

/* Include private header of superclass */
#include &lt;BaseTypeP.h>

/* top_ph_file */
/* New representation types used by the FooThing widget */

/* Declarations for class functions */
extern void FooThingFrubble(Widget /* widget */);
extern void PointedToFunc(int /* a */, int /* b */);
extern void TestFunc(Widget /* widget */);


/* Defines for inheriting superclass function pointer values */
#ifndef XtInheritFuncptr
# define XtInheritFuncptr ((void (*)(int /* a */, int /* b */)) _XtInherit)
#endif
#ifndef XtInheritTestFunc
# define XtInheritTestFunc ((XtWidgetProc) _XtInherit)
#endif
#ifndef XtInheritFuncInExtension
# define XtInheritFuncInExtension ((XtWidgetProc) _XtInherit)
#endif


/* New fields for the FooThing instance record */
typedef struct {
    /* Settable resources and private data */
    Pixel foreground; /* comment on foreground */
    Pixel background; 
    int testint; 
    long int long_int_field; 

} FooThingPart;

/* Full instance record declaration */
typedef struct FooThingRec {
    BaseTypePart base_type;
    FooThingPart foo_thing;

} FooThingRec;

/* Types for FooThing class methods */

/* New fields for the FooThing class record */
typedef struct {
    int foo_thing_int; 
    long foo_thing_long; 
    void (*funcptr)(int /* a */, int /* b */); 
    XtWidgetProc test_func; 

} FooThingClassPart;

/* Class extension records */
typedef struct  {
    XtPointer next_extension;   /* 1st 4 required for all extension records */
    XrmQuark record_type;       /* FooQuark; when on FooThingClassPart */
    long version;               /* must be FooThingExtensionFooQuark3Version */
    Cardinal record_size;       /* sizeof(FooThingClassExtensionFooQuark3Rec) */
    XtWidgetProc func_in_extension; 

} FooThingClassExtensionFooQuark3Rec, *FooThingClassExtensionFooQuark3;

#define FooThingExtensionFooQuark3Version 3

typedef struct  {
    XtPointer next_extension;   /* 1st 4 required for all extension records */
    XrmQuark record_type;       /* NULLQUARK; when on FooThingClassPart */
    long version;               /* must be FooThingExtensionVersion */
    Cardinal record_size;       /* sizeof(FooThingClassExtensionRec) */
    int NULLQUARK; /* default extension record name is NULLQUARK */

} FooThingClassExtensionRec, *FooThingClassExtension;

#define FooThingExtensionVersion 1



/* Full class record declaration */
typedef struct FooThingClassRec {
    BaseTypeClassPart base_type_class;
    FooThingClassPart foo_thing_class;

} FooThingClassRec;

extern struct FooThingClassRec fooThingClassRec;

/* bottom_ph_file */
#endif /* FOOTHINGP_H */
</code></pre>

### FooThing.c

<pre><code>
/*
 * Do not edit this file.
 * It has been generated by TwiXt
 * from widget description testfile.xt.
 */

#include &lt;stddef.h>
#include &lt;FooThingP.h>

/* top_c_file */
/******************************************************************
 *
 * FooThing Resources
 *
 ******************************************************************/

static XtResource resources[] = {
    { /* comment on foreground */
        .resource_name   = XtNforeground,
        .resource_class  = XtCColor,
        .resource_type   = XtRPixel,
        .resource_size   = sizeof (Pixel),
        .resource_offset = XtOffsetOf(FooThingRec, foo_thing.foreground),
        .default_type    = XtRImmediate,
        .default_addr    = (XtPointer)(0),
    },
    { 
        .resource_name   = XtNbackground,
        .resource_class  = XtCColor,
        .resource_type   = XtRPixel,
        .resource_size   = sizeof (Pixel),
        .resource_offset = XtOffsetOf(FooThingRec, foo_thing.background),
        .default_type    = XtRImmediate,
        .default_addr    = (XtPointer)((0 + 0) / 2),
    },
    { 
        .resource_name   = XtNtestint,
        .resource_class  = XtCInt,
        .resource_type   = XtRInt,
        .resource_size   = sizeof (int),
        .resource_offset = XtOffsetOf(FooThingRec, foo_thing.testint),
        .default_type    = XtRImmediate,
        .default_addr    = (XtPointer)(3),
    },

};

struct FooThingClassRec fooThingClassRec = {
    { /* BaseType for FooThing */
    .classfield_int = 99, 
    .classfield_long = 4, 
    .wibble = NonDefaultNameForInheritWibble, 
    .frubble = FooThingFrubble, 
    }, /* BaseType */
    { /* FooThing for FooThing */
    .foo_thing_int = 0xF001, 
    .foo_thing_long = 0xF002, 
    .funcptr = PointedToFunc, 
    .test_func = TestFunc, 
    }, /* FooThing */

};

/*
 * Declare this as WidgetClass instead of the "more real" types
 *
 *     FooThingClassRec * or BaseTypeClassRec *,
 *
 * because Xt functions such as XtCreateWidget() take an argument of that type.
 *
 * The definition of WidgetClass in &lt;X11/Core.h> is
 *
 *     typedef struct _WidgetClassRec *CoreWidgetClass;
 *
 * where Widget is a strange alias of Core.
 */
WidgetClass fooThingWidgetClass = (WidgetClass)&fooThingClassRec.base_type_class;

/* Definitions for class functions */
void FooThingFrubble(Widget  widget )
{

}

void PointedToFunc(int  a , int  b )
{

}

void TestFunc(Widget  widget )
{
/* This is to be the body of the function pointed to by WidgetProc test_func */
        return NULL;
}



/* bottom_c_file */
</code></pre>

<!--
 vim:expandtab:ft=markdown:
-->
